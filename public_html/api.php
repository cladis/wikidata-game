<?PHP

require_once ( 'php/common.php' ) ;

header('Content-type: application/json');

$db = openToolDB ( 'merge_candidates' ) ;
$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
$action = get_request ( 'action' , '' ) ;
$user = $db->real_escape_string ( trim ( get_request ( 'user' , '' ) ) ) ;

$out = array ( 'status' => 'OK' ) ;

function doesItemExist ( $item ) { // numeric
	global $dbwd ;
	$ret = false ;
	$sql = "select * from page WHERE page_namespace=0 AND page_title='Q$item'" ;
	if(!$result = $dbwd->query($sql)) die('1 There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){ $ret = true ; }
	return $ret ;
}

function doesItemHaveLink ( $item , $target ) {
	global $dbwd ;
	$ret = false ;
	$ns = 0 ;
	if ( substr($target,0,1) == 'P' ) $ns = 120 ;
	$sql = "select * from wb_entity_per_page,pagelinks where epp_entity_id=$item AND epp_entity_type='item' AND epp_page_id=pl_from AND pl_namespace=$ns AND pl_title='$target' LIMIT 1" ;
	if(!$result = $dbwd->query($sql)) die('2 There was an error running the query [' . $db->error . ']'.$sql);
	while($o = $result->fetch_object()){ $ret = true ; }
	return $ret ;
}

function getUserID ( $user ) {
	global $db ;
	$ret = '' ;
	if ( $user == '' ) return 0 ;
	$sql = "SELECT * FROM users WHERE name='$user'" ;
	if(!$result = $db->query($sql)) die('3 There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$ret = $o->id ;
	}
	if ( $ret != '' ) return $ret * 1 ;
	
	$sql = "INSERT INTO users (name) VALUES ('$user')" ;
	if(!$result = $db->query($sql)) die('4 There was an error running the query [' . $db->error . ']');
	$uid = getUserID ( $user ) ;
	$sql = "INSERT IGNORE INTO scores (user) VALUES ($uid)" ;
	if(!$result = $db->query($sql)) die('4a There was an error running the query [' . $db->error . ']');

	return $uid ;
}


function inc_score ( $user_id , $field ) { // $user_id and $field are trusted
	global $db ;
	$sql = "UPDATE scores SET $field=$field+1 WHERE user=$user_id" ;
	if(!$result = $db->query($sql)) die('5 There was an error running the query [' . $db->error . ']');
}

function deleteGameRow ( $table , $id ) {
	global $db ;
	$sql = "DELETE FROM $table WHERE status IS NULL AND id=" . $x->id ;
	if(!$result = $db->query($sql)) die('6 There was an error running the query [' . $db->error . ']');
}

function checkSpecialConitions ( $table , $x ) { // Verifies special conditions for game candidates. Returns true if all is OK.
	global $db , $dbwd ;

	if ( $table == 'get_genderless_person' ) {
		if ( !doesItemExist($x->item) or doesItemHaveLink($x->item,'P21') ) { // Check for gender link
			deleteGameRow ( $table , $x->id ) ;
			return false ;
		}
	}

	if ( $table == 'potential_people' ) {
		if ( !doesItemExist($x->item) or doesItemHaveLink($x->item,'P31') or doesItemHaveLink($x->item,'P131') 
			 or doesItemHaveLink($x->item,'P105') or doesItemHaveLink($x->item,'P171') or doesItemHaveLink($x->item,'P17') 
		) { // Check for contra-indicative properties
			deleteGameRow ( $table , $x->id ) ;
			return false ;
		}
	}
	
	if ( table == 'item_pairs' ) {
		if ( !doesItemExist($x->item1) or !doesItemExist($x->item2) ) { // One item is deleted
			deleteGameRow ( 'item_pairs' , $x->id ) ;
			return false ;
		} else if ( doesItemHaveLink($x->item1,'Q4167410') or doesItemHaveLink($x->item2,'Q4167410') ) { // One is a disambiguation page
			$sql = "UPDATE item_pairs SET status='DIS' WHERE id=" . $x->id ;
			if(!$result = $db->query($sql)) die('8 There was an error running the query [' . $db->error . ']');
			return false ;
		}
	}
	
	return true ; // Default
}



/********************************************************************************************************************************************
 action
*********************************************************************************************************************************************/

// K=>V is table_name => mode_name
$tables = array ( 'genderless_people' => 'nogender' , 'item_pairs' => 'merge' , 'potential_people' => 'person' ) ;


if ( $action == 'get_candidate' ) {

	$table = $db->real_escape_string ( trim ( get_request ( 'table' , '' ) ) ) ;
//$test = ( $table == 'genderless_people' and get_request('test','') == '1' ) ;
	while ( 1 ) {
		$r = rand() / getrandmax() ;
//		$sql = "select * from $table where status is null order by rand() limit 1" ;
		$sql = "SELECT * FROM $table WHERE status IS NULL AND random >= $r ORDER BY random LIMIT 1" ;
		if(!$result = $db->query($sql)) die('11 There was an error running the query [' . $db->error . '] '.$sql);
		$x = $result->fetch_object() ;
		if ( checkSpecialConitions ( $table , $x ) ) {
			$out['data'] = $x ;
			break ;
		}
	}

} else if ( $action == 'set_status' ) {

	$uid = getUserID($user) ;
	$id = get_request ( 'id' , '' ) * 1 ;
	$status = $db->real_escape_string ( trim ( get_request ( 'status' , '' ) ) ) ;
	$table = $db->real_escape_string ( trim ( get_request ( 'table' , '' ) ) ) ;
	$ts = date ( 'YmdHis' ) ;
	
	if ( $id > 0 and $uid > 0 and $status != '' ) {
		$sql = "UPDATE $table SET status='$status',user=$uid,timestamp='$ts' WHERE id=$id AND status IS NULL" ;
		if(!$result = $db->query($sql)) die('10 There was an error running the query [' . $db->error . ']');
	}
	inc_score ( $uid , $table ) ;

} else if ( $action == 'stats' ) {

	$sql = "SELECT count(*) as cnt from users" ;
	if(!$result = $db->query($sql)) die('20 There was an error running the query [' . $db->error . '] '.$sql);
	$x = $result->fetch_object() ;
	$out['players'] = $x->cnt ;

	$uid = getUserID($user) ;
	$sql = "SELECT * FROM scores WHERE user=$uid" ;
	if(!$result = $db->query($sql)) die('20 There was an error running the query [' . $db->error . '] '.$sql);
	$us = $result->fetch_object() ;
	foreach ( $tables AS $t => $mode ) {
		$out['data'][$mode] = array() ;
		$out['data'][$mode]['your_score'] = $us->$t ;

		$sql = "SELECT count(*) AS cnt FROM $t WHERE status IS NULL" ;
		if(!$result = $db->query($sql)) die('20 There was an error running the query [' . $db->error . '] '.$sql);
		$x = $result->fetch_object() ;
		$out['data'][$mode]['todo'] = $x->cnt ;

		$sql = "SELECT sum($t) AS s FROM scores" ;
		if(!$result = $db->query($sql)) die('20 There was an error running the query [' . $db->error . '] '.$sql);
		$x = $result->fetch_object() ;
		$out['data'][$mode]['done_all'] = $x->s ;

		$out['data'][$mode]['top10'] = array() ;
		$sql = "SELECT * FROM scores,users WHERE users.id=scores.user ORDER BY $t DESC LIMIT 10" ;
		if(!$result = $db->query($sql)) die('20 There was an error running the query [' . $db->error . '] '.$sql);
		while ( $x = $result->fetch_object() ) {
			$out['data'][$mode]['top10'][] = array ( 'user' => $x->name , 'score' => $x->$t ) ;
		}
		
		$sql = "SELECT count(DISTINCT $t)+1 AS cnt FROM scores WHERE $t > " . $us->$t ;
		if(!$result = $db->query($sql)) die('20 There was an error running the query [' . $db->error . '] '.$sql);
		$x = $result->fetch_object() ;
		$out['data'][$mode]['rank'] = $x->cnt ;
	}

} else {
	$out['status'] = "Unknown action $action" ;
}

print json_encode ( $out ) ;

?>